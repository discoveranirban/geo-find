
const Message = require('../models/message');


exports.signin = async function(req, res, next) {

  Message.find({
    location: {
     $near: {
      $maxDistance: 1000,
      $geometry: {
       type: "Point",
       coordinates: [88.224894,22.499909]
      }
     }
    }
   }).find((error, results) => {
    if (error) console.log(error);
    console.log(JSON.stringify(results, 0, 2));
    res.send(JSON.stringify(results, 0, 2))
   });

}

exports.signup = function(req, res, next) {

  const username = req.body.username;
  const coordinates = req.body.coordinates;

  var message = new Message({
    username: username,
    location: {
     type: "Point",
     coordinates: coordinates
    },
   });

   message.save((err, message) => {
    if (err) console.log(err);
    console.log(message);
   });
}
