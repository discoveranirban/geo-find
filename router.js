
const Authentication = require('./controllers/authentication');

module.exports = function(app) {
  app.get('/', function(req, res) {
    res.send({ hi: 'there' });
  });
  app.post('/signup', Authentication.signup);
  app.get('/signin', Authentication.signin);
}
